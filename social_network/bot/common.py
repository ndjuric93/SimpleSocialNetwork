''' Helper module for helper functions for bot'''
import os
import random
import json

import yaml

from faker import Faker
from pyhunter import PyHunter

HUNTER_KEY = os.environ['HUNTER_KEY']

def find_random_emails(company='Delta', limit=10, emails_type='personal'):
    """
    Find random email from company

    Parameters
    ----------
    company: str
        Get email of the comapny

    limit: int
        Number of emails

    emails_type: str
        Type of email to be generated

    Returns
    -------
    list
        Random generated emails

    """
    hunter = PyHunter(HUNTER_KEY)
    return [val['value'] for val in hunter.domain_search(
        company=company, limit=limit, emails_type=emails_type)['emails']]

def get_random_email(company='Delta', limit=10, emails_type='personal'):
    """
    Fetch random emails from given company

    Parameters
    ----------
    company: str
        Get email of the comapny

    limit: int
        Number of emails

    emails_type: str
        Type of email to be generated

    Returns
    -------
    str
        Random generated email

    """
    emails = find_random_emails(company=company, limit=limit, emails_type=emails_type)
    while len(emails) is not 0:
        yield emails.pop(random.randint(0, len(emails)-1))

def generate_random_user_data(email):
    '''Generates random user data '''
    return {
        'username' : Faker().profile(fields=['username'])['username'],
        'password' : Faker().word(),
        'email' : email
    }

def get_random_user_data(count=10):
    """
    Generate random users data

    Parameters
    ----------
    count : int
        Number of users to be generated

    Returns
    -------
    dict
        Dictionary containting random user data

    """
    for email in get_random_email(limit=count):
        yield generate_random_user_data(email)

def get_random_text(word_count=4):
    """
    Generate random text with given word count

    Parameters
    ----------
    word_count : int
        Maximum number of words for the generated sentence

    Returns
    -------
    str
        Random generated sentence

    """
    return Faker().sentence(nb_words=word_count)

def load_yaml(path, mode='r'):
    """
    Load yaml file

    Parameters
    ----------
    path : str
        Path to file

    mode : str
        Mode for opening file

    Returns
    -------
    dict
        Yaml file content

    """
    with open(path, mode) as file:
        return yaml.load(file)

def get_response(response):
    """
    Get json response

    Parameters
    ----------
    response : bytes
        Django response content

    Returns
    -------
    dict
        Response

    """
    return json.loads(response.content)

def get_field(response, field):
    """
    Get field of the json response

    Parameters
    ----------
    response : bytes
        Django response content

    field : str
        Field to retrieve from request content

    Returns
    -------
    str
        Response field

    """
    return json.loads(response.content)[field]
