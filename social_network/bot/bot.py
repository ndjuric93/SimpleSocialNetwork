""" Module containing bot class for social_network_app """
import random

from collections import defaultdict, Counter

from common import get_random_user_data, get_random_text
from common import load_yaml, get_field, get_response

from api import signup, login, send_post, get_posts, like

class Bot(object):
    """ Bot class """
    def __init__(self):
        config = load_yaml('config.yml')
        self.user_count = config['number_of_users']
        self.max_posts_per_user = config['max_posts_per_user']
        self.max_likes_per_user = config['max_likes_per_user']
        self.users = {}
        self.user_ids = {}
        self.user_token = {}
        self.user_likes = defaultdict(lambda: [])
        print('! Bot started !')

    def run_bot(self):
        """ Run bot operation """
        print('\n *** Signing up phase! ***\n')
        self.signup_users()
        print('\n *** Posting phase! ***\n')
        self.generate_random_posts()
        print('\n *** Like phase! ***\n')
        self.like_activity()

    def signup_users(self):
        """ Signup and login random generated users """
        for data in get_random_user_data(count=self.user_count):
            response = signup(data)
            username = get_field(response, 'username')
            self.users[username] = data['password']
            self.user_ids[username] = get_field(response, 'id')
            print('Signing up ', data['username'])

        for user, password in self.users.items():
            response = login({'username' : user, 'password' : password})
            self.users[user] = get_field(response, 'token')
            self.user_token[self.user_ids[user]] = get_field(response, 'token')
            self.selected_user = user
            print('Logging in ', user)

    def generate_random_posts(self):
        """ Generate random posts """
        for user, token in self.users.items():
            for _ in range(random.randint(0, self.max_posts_per_user)):
                content = get_random_text()
                send_post({'content' : content, 'user' : self.user_ids[user]}, token)
                print('User ', user, ' is posting ', content)

    def like_activity(self):
        """ Liking activity of te bot """
        posts = self.get_post_list()

        while self.bot_should_run():
            next_user_to_like = self.get_next_user_to_like(posts)
            while len(self.user_likes[next_user_to_like]) <= self.max_likes_per_user:
                likeable_posts = self.get_likeable_posts(self.get_post_list(), next_user_to_like)
                if not likeable_posts:
                    print('User ', next_user_to_like, ' has no posts to like, though bot task is not completed.')
                    print('Re run the bot please.')
                    break
                    
                post = random.choice(likeable_posts)
                response = like(post_id=post['id'],
                                token=self.user_token[next_user_to_like])
                if response.status_code == 202:
                    self.user_likes[next_user_to_like].append(post['id'])
                    print('User with ID ', next_user_to_like, ' successfully liked post with ID', post['id'])
        print('Bot finished!')


    def get_next_user_to_like(self, post_data):
        """
        Get id of the next user to perform like operations

        Returns
        -------
        int
            User id of the next user to like if available, None otherwise

        """
        counter = Counter(k['user'] for k in post_data)
        for user, _ in counter.most_common():
            if len(self.user_likes[user]) < self.max_likes_per_user:
                return user
        return None

    def bot_should_run(self):
        """
        Gets status if the bot should run

        Returns
        -------
        Boolean
            Returns if the bot should run

        """
        available_posts = list(filter(lambda post: post['user'] in self.user_ids.values(),
                                      get_response(get_posts(self.users[self.selected_user]))))
        return (True if list(filter(lambda post: post['like_count'] == 0, available_posts))
                and self.get_next_user_to_like(available_posts) is not None else False)

    def get_post_list(self):
        """
        Get posts from server

        Returns
        -------
        list
            List of posts on the server

        """
        posts = get_response(get_posts(self.users[self.selected_user]))
        return list(filter(lambda post: post['user'] in self.user_ids.values(), posts))

    def get_likeable_posts(self, posts, next_user_to_like):
        """
        Get likeable posts for given user

        Parameters
        ----------
        posts : list
            List of available posts from the server

        next_user_to_like : int
            Id of the  next user to like

        Returns
        -------
        list
            List of posts available to like by the given user

        """
        available_posts = list(filter(lambda post: post['user'] != next_user_to_like, posts))
        available_posts = list(filter(lambda post: post['id'] not in self.user_likes[next_user_to_like], available_posts))
        return list(filter(lambda post: post['like_count'] == 0, available_posts))

if __name__ == '__main__':
    Bot().run_bot()
