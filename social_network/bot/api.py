""" Module containing api request methods for social_network_app """
import requests

SERVER_URL = 'http://localhost:8000/'

def signup(data):
    """
    Signup request for social_network_app

    Parameters
    ----------
    data : dict
        Dictionary containing payload data

    Returns
    -------
    int
        Description of return value

    """
    return requests.post(SERVER_URL + 'signup/', data=data)

def login(data):
    """
    Login request for social_network_app

    Parameters
    ----------
    data : dict
        Dictionary containing payload data

    Returns
    -------
    int
        Request response

    """
    return requests.post(SERVER_URL + 'login/', data=data)

def send_post(data, token):
    """
    Send post request for social_network_app

    Parameters
    ----------
    token : str
        JWT Token for authorization

    data : dict
        Dictionary containing payload data

    Returns
    -------
    int
        Request response

    """
    return requests.post(SERVER_URL + 'posts/',
                         headers={'Authorization': 'JWT ' + token},
                         data=data)

def get_posts(token):
    """
    Get posts request for social_network_app

    Parameters
    ----------
    token : str
        JWT Token for authorization

    Returns
    -------
    int
        Request response

    """
    return requests.get(
        SERVER_URL + 'posts/',
        headers={'Authorization': 'JWT ' + token},
    )

def like(post_id, token):
    """
    Like post request for social_network_app

    Parameters
    ----------
    post_id : int
        Id of the post to be liked

    token : str
        JWT Token for authorization

    Returns
    -------
    int
        Request response

    """
    return requests.post(
        SERVER_URL + 'posts/' + str(post_id) + '/like/',
        headers={'Authorization': 'JWT ' + token}
    )
