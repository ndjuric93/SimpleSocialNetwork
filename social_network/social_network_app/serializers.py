'''Module for serializers of models'''
from django.contrib.auth.models import User

from rest_framework import serializers
from social_network_app.models import Post

class PostSerializer(serializers.ModelSerializer):
    """
    Serializer to map the User instance into JSON format.
    """

    class Meta:
        """
        Meta class to map serializer's field with the model's fields
        """
        model = Post
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    """
    Serializer to map the User instance into JSON format.

    Serializer contains password, posts that this user did, first and last name
    """

    password = serializers.CharField(write_only=True)
    posts = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    first_name = serializers.ReadOnlyField(allow_null=True)
    last_name = serializers.ReadOnlyField(allow_null=True)

    class Meta:
        """
        Meta class to map serializer's field with the model's fields
        """
        model = User
        fields = ('id', 'username', 'password', 'email', 'first_name', 'last_name', 'posts')

    def create(self, validated_data):
        """
        Method for creating user out of validated data

        Parameters
        ----------
        validate_data : dict
            Dictionary containing validated data

        Returns
        -------
        User
            Created user object

        """
        user = User.objects.create_user(
            username=validated_data['username'],
            email=validated_data['email'],
            password=validated_data['password'],
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name']
        )
        user.save()

        return user
