""" Helper module for helper functions for social network app """
import os

import clearbit
from pyhunter import PyHunter

clearbit.key = os.environ['CLEARBIT_KEY']

def get_enrichment_data(email):
    """
    Get additional data for given email

    Parameters
    ----------
    email : str
        Email to fetch additional data for

    Returns
    -------
    dict
        Dictionary containing enriched user data

    """
    return clearbit.Enrichment.find(email=email, stream=True)

HUNTER_KEY = os.environ['HUNTER_KEY']

def is_verified(email):
    """
    Verify given email

    Parameters
    ----------
    email : str
        Email to be verified

    Returns
    -------
    Boolean
        Verification status

    """
    hunter = PyHunter(HUNTER_KEY)
    return hunter.email_verifier(email)['result'] != 'undeliverable'

def get_name_by_email(email):
    """
    Get full name of the given email


    Parameters
    ----------
    email : str
        Email to be enriched

    Returns
    -------
    dict
        First and last name retrieved from the email

    """
    enrichment = get_enrichment_data(email)['person']
    first_name = (enrichment['name']['givenName'] if enrichment is not None
                  and enrichment['name'] is not None else '')
    last_name = (enrichment['name']['familyName'] if enrichment is not None
                 and enrichment['name'] is not None else '')
    return {
        'first_name' : first_name if first_name is not None else '',
        'last_name' : last_name if last_name is not None else ''
    }
