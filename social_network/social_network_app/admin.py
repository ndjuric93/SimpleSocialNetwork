""" Module for admin in Social Network App """
from django.contrib import admin

from social_network_app.models import Post

admin.site.register(Post)
