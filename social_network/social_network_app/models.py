'''Module for Models in Social Network App'''
from django.db import models
from django.contrib.auth.models import User

class Post(models.Model):
    """
    Model for social network post

    This model contains user, content, and number of likes
    """

    user = models.ForeignKey(User, related_name='posts', on_delete=models.CASCADE)
    content = models.CharField(max_length=1024)
    like_count = models.IntegerField(default=0)

class UserLikes(models.Model):
    """
    Model for user likes.

    This model is used to keep track of users who already liked post.
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
