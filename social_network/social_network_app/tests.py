""" Module for testing of Social Network Models """
from django.test import TestCase

from rest_framework import status
from rest_framework.test import APIClient
from rest_framework.reverse import reverse

from rest_framework_jwt.compat import get_user_model

from social_network_app.models import Post
from social_network_app.serializers import UserSerializer

User = get_user_model()

class PostModelTests(TestCase):
    """ Class for testing Post Model """

    def setUp(self):
        """ Set up for creating post test case """
        User = get_user_model()
        self.user = User(username='test@test.com', password='pass')
        self.user.save()

    def test_can_create_post(self):
        """ Test if post can be created """
        post = Post(user=self.user,
                    content='test_post')
        post.save()
        self.assertEqual(Post.objects.count(), 1)

class SignupTestCase(TestCase):
    """ TestCase for testing signup """

    def setUp(self):
        """ Set up for signup test case """
        self.client = APIClient()
        self.username = 'tester'
        self.email = 'tester@gmail.com'
        self.password = 'my_password'
        self.user_data = {
            'username' : self.username,
            'email' : self.email,
            'password' : self.password
        }
        self.signup_response = self.client.post(
            reverse('signup'),
            self.user_data,
            format='json'
        )

    def test_api_signup_a_user(self):
        """ Test for testing signing up a user """
        self.assertEqual(self.signup_response.status_code, status.HTTP_201_CREATED)

class BaseTestCase(TestCase):
    """ TestCase for testing signup """

    def setUp(self):
        """ Set up for base test case """
        self.client = APIClient()
        self.username = 'tester'
        self.email = 'tester@gmail.com'
        self.password = 'my_password'
        self.user_data = {
            'username' : self.username,
            'email' : self.email,
            'password' : self.password
        }
        self.client.post(
            reverse('signup'),
            self.user_data,
            format='json'
        )

        login_response = self.client.post(
            reverse('login'),
            self.user_data,
            format='json'
        )
        self.token = login_response.data['token']
        self.user = User.objects.filter(username=self.username, email=self.email).get()

class PostBaseTestCase(BaseTestCase):
    """ Base test case for post related operations """

    def setUp(self):
        """ Set up for base test case """
        super(PostBaseTestCase, self).setUp()

        self.post_data = {
            'user' : self.user.pk,
            'content' : 'Lorem Ipsum'
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        self.response = self.client.post(
            reverse('posts-list'),
            self.post_data,
            format='json'
        )

class PostTestCase(PostBaseTestCase):
    """ Test case for post related operations """

    def test_api_send_a_post(self):
        """ Test for testing sending a post """
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

class LikeTestCase(PostBaseTestCase):
    """ TestSuite for liking posts """

    def setUp(self):
        """
        Set up for like test cases
        """
        super(LikeTestCase, self).setUp()
        post = Post.objects.get(user=self.user.pk)

        self.like_data = {
            'post_id' : '1'
        }
        self.like_response = self.client.post(
            reverse('posts-like', kwargs={'pk' : post.id}),
            self.like_data,
            format='json'
        )

    def test_api_like_a_post(self):
        """ Test for testing liking a post """
        self.assertEqual(self.like_response.status_code, status.HTTP_202_ACCEPTED)

class UnlikeTestCase(PostBaseTestCase):
    """ TestSuite for unliking posts """

    def setUp(self):
        """ Set up for base test case """
        super(UnlikeTestCase, self).setUp()
        post = Post.objects.get(user=self.user.pk)
        self.unlike_data = {
            'post_id' : '1'
        }
        self.unlike_response = self.client.post(
            reverse('posts-unlike', kwargs={'pk' : post.id}),
            self.unlike_data,
            format='json'
        )

    def test_api_unlike_a_post(self):
        """ Test for testing unliking a post"""
        self.assertEqual(self.unlike_response.status_code, status.HTTP_202_ACCEPTED)
