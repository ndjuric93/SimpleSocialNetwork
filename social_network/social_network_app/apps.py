""" Module for Social Networking Django Application """
from django.apps import AppConfig

class SocialNetworkAppConfig(AppConfig):
    """ Django App config for Social Network Application """
    name = 'social_network_app'
