""" Module for urls in the Social Network application """
from django.conf.urls import url
from django.conf.urls import include

from rest_framework.routers import DefaultRouter
from rest_framework_jwt.views import obtain_jwt_token

from social_network_app.views import SignupView, PostViewSet

ROUTER = DefaultRouter()
ROUTER.register(r'posts', PostViewSet, base_name='posts')

urlpatterns = [
    url(r'^signup/$', SignupView.as_view(), name='signup'),
    url(r'^login/', obtain_jwt_token, name='login'),
    url(r'^', include(ROUTER.urls, ))
]
