""" Module for Views in Social Network App """
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseBadRequest
from django.core.exceptions import ValidationError

from rest_framework import generics
from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.permissions import IsAuthenticated

from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from social_network_app.serializers import UserSerializer, PostSerializer
from social_network_app.models import Post, UserLikes
from social_network_app.common import is_verified, get_name_by_email

class SignupView(generics.CreateAPIView):
    """ View for Signup request """
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def perform_create(self, serializer):
        """
        Method for creating user out of validated data.

        Signup view receives username, email and password.
        Method verifies the email, gets enriched data, and saves
        the data.

        Parameters
        ----------
        serializer : dict
            Serializer of a data sent to signup view

        """
        if is_verified(serializer.validated_data['email']) is not True:
            return HttpResponse("Failure", status=400)
        mail_data = get_name_by_email(serializer.validated_data['email'])
        serializer.save(
            first_name=mail_data['first_name'],
            last_name=mail_data['last_name']
        )

class PostViewSet(viewsets.ModelViewSet):
    """ ViewSet for post related operations """
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    authentication_class = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    @detail_route(['post'])
    def like(self, request, pk=None):
        """
        Route for liking post, with POST request.

        Parameters
        ----------
        request : Request
            POST http request

        pk : int
            Primary key representing post id

            Returns
        -------
        HttpResponse
            Response containing status

        """
        try:
            self.check_if_user_liked(False)
        except ValidationError as error:
            return HttpResponseBadRequest(error)
        self.update_like_count(1)
        post = self.get_object()
        UserLikes.objects.create(user_id=post.user_id, post_id=post.id)
        return HttpResponse('Success', status=202)

    @detail_route(['post'])
    def unlike(self, request, pk=None):
        """
        Route for unliking post, with POST request.

        Parameters
        ----------
        request : Request
            POST http request

        pk : int
            Primary key representing post id

            Returns
        -------
        HttpResponse
            Response containing status

        """
        try:
            self.check_if_user_liked(False)
        except ValidationError as error:
            return HttpResponseBadRequest(error)
        self.update_like_count(-1)
        post = self.get_object()
        UserLikes.objects.exclude(user_id=post.user_id, post_id=post.id)
        return HttpResponse('Success', status=202)

    def perform_create(self, serializer):
        """
        Method for creating post on server.

        Parameters
        ----------
        serializer : dict
            Serializer of a data sent to PostViewSet

        """
        serializer.save()

    def check_if_user_liked(self, expected):
        """
        Checks if user has already liked the post

            expected : Boolean value representing the call operation

        """
        post = self.get_object()
        status = UserLikes.objects.filter(user_id=post.user_id, post_id=post.id)
        if status.exists() != expected:
            raise ValidationError('User cannot do operation')

    def update_like_count(self, count):
        """
        Updates like count of the post object

        Parameters
        ----------
        count : int
            Number to add to like_count

        """
        post = self.get_object()
        post.like_count += count
        post.save()
