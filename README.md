# Simple Social Network using Django Framework

This project represents simple REST API based on Django framework for social network.

Social network consists of signing up users, login, creating, liking and unliking posts.

There is a small set of tests to test api response, and there is a small bot implementation that serves as an end to end test of the API.

# USAGE

To use this project, you need python 3.6 and packages listed in requirements.txt. 

Preferably virtualenv should be used.

To install packages, you should run the ```pip install -r requirements.txt```.

This server uses Hunter.Io API for getting emails and verifying them, and it uses Clearbit Enrichment API for fetching additional Email data(first and last name of the user)
User should export ```CLEARBIT_KEY``` and ```HUNTER_KEY``` environmental variables to the keys he got from the websites.

To set up database you should run
 
```python manage.py makemigrations```

```python manage.py migrate```

After that, database should be set up and you can run the server.

To test the server, you can run:

```python manage.py test```

# REST API

API has the following requests

#### signup

Signing up of a user using Django user model.

#### login

Login of a user, returns JWT token.

#### create_post

Create post generates a random post and sends it to the server

#### like_post

Like post increase like count on a particular post

#### unlike_post

Unlike post decreases like count on a particular post

# BOT

Bot is a simple end to end test which generates random users, signs them up and login them. 
Each user posts a random number of posts, and then the like game starts.
User picks up and likes a random post until he reaches maximum number of likes. Then the next user takes a turn.

Number of maximum likes, maximum posts, and user count is found in config.yml file.

To start the bot, go into the bot directory and run ```python bot.py```
